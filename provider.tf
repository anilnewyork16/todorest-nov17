provider "google" {
  credentials = file("/var/lib/jenkins/sc.json")
  project     = "sg-project-1-264708"
  region      = "us-central1"
}